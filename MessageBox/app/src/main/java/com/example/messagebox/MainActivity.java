package com.example.messagebox;



import android.app.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    Button alert;
    Button toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alert = (Button)findViewById(R.id.alert);
        alert.setOnClickListener(this);
        toast = (Button)findViewById(R.id.toast);
        toast.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == alert){
            new AlertDialog.Builder(this).setTitle("Message Title")
                    .setMessage("Ini Adalah bagian Message")
                    .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
            .show();
        } else{
            Toast.makeText(this, "Hai Message Toast", Toast.LENGTH_SHORT).show();
        }
    }
}
